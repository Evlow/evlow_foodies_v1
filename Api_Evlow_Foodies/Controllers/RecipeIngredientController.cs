﻿using Api.Evlow_Foodies.Buisness.DTO;
using Api.Evlow_Foodies.Buisness.Service.Contract;
using Api.Evlow_Foodies.Datas.Entities.Entities;
using Microsoft.AspNetCore.Mvc;

namespace Api_Evlow_Foodies.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RecipeIngredientController : ControllerBase
    {
        /// <summary>
        ///  Le service de gestion des unités de mesure
        /// </summary>
        private readonly IRecipeIngredientService _recipeService;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnityController"/> class.
        /// </summary>
        /// <param name="unityService">The unite service.</param>
        public RecipeIngredientController(IRecipeIngredientService recipeService)
        {
            _recipeService = recipeService;
        }

        // GET api/Unites
        /// <summary>
        /// Ressource pour récupérer la liste des unités de mesure.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(List<RecipeIngredientDTO>), 200)]
        public async Task<ActionResult> GetRecipeIngredientsAsync()
        {
            var recipes = await _recipeService.GetRecipeIngredientAsync().ConfigureAwait(false);

            return Ok(recipes);
        }

        // GET api/Unites
        /// <summary>
        /// Ressource pour récupérer la un Id d'une unité de mesure
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(RecipeIngredientDTO), 200)]
        public async Task<ActionResult> ReccipeId(int id)
        {
            try
            {
                var recipeId = await _recipeService.GetRecipeIngredientIdAsync(id).ConfigureAwait(false);

                return Ok(recipeId);
            }
            catch (Exception e)
            {
                return BadRequest(new
                {
                    Error = e.Message,
                });
            }

        }

        // POST api/Unites
        /// <summary>
        /// Ressource pour créer une nouvelle unité de mesure.
        /// </summary>
        /// <param name="unity">les données de l'unité à créer</param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(RecipeIngredient), 200)]
        //public async Task<ActionResult> CreateUnityAsync([FromBody] RecipeIngredient recipe)
        //{
        //    if (string.IsNullOrWhiteSpace(recipe.RecipeIngredientQuantity))
        //    {
        //        return Problem("Echec : nous avons un nom d'unité de mesure vide !!");
        //    }

        //    try
        //    {
        //        var recipeAdded = await _recipeService.CreateRecipeIngredientAsync(recipe).ConfigureAwait(false);

        //        return Ok(recipeAdded);
        //    }
        //    catch (Exception e)
        //    {
        //        return BadRequest(new
        //        {
        //            Error = e.Message,
        //        });
        //    }

        //}

        // PUT api/Unites/1
        /// <summary>
        /// Ressource pour mettre à jour une unité de mesure.
        /// </summary>
        /// <param name="id">L'identifiant de l'unité.</param>
        /// <param name="unite">les données modifiées.</param>
        /// <returns></returns>
        //[HttpPut("{id}")]
        //[ProducesResponseType(typeof(RecipeIngredient), 200)]
        //public async Task<ActionResult> UpdateUniteAsync(int id, [FromBody] RecipeIngredient recipe)
        //{
        //    if (string.IsNullOrWhiteSpace(recipe.RecipeIngredientTitle))
        //    {
        //        return Problem("Echec : nous avons un nom d'unité de mesure vide !!");
        //    }

        //    try
        //    {
        //        var recipeUpdated = await _recipeService.UpdateRecipeIngredientAsync(id, recipe).ConfigureAwait(false);

        //        return Ok(recipeUpdated);
        //    }
        //    catch (Exception e)
        //    {
        //        return BadRequest(new
        //        {
        //            Error = e.Message,
        //        });
        //    }

        //}

        // DELETE api/Unites/1
        /// <summary>
        /// Ressource pour supprimer une unité de mesure.
        /// </summary>
        /// <param name="id">L'identifiant de l'unité.</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(RecipeIngredientDTO), 200)]
        public async Task<ActionResult> DeleteRecipeIngredientyAsync(int id)
        {
            try
            {
                var recipeDeleted = await _recipeService.DeleteRecipeIngredientAsync(id).ConfigureAwait(false);

                return Ok(recipeDeleted);
            }
            catch (Exception e)
            {
                return BadRequest(new
                {
                    Error = e.Message,
                });
            }

        }

    }
}