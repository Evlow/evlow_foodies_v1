﻿using Api.Evlow_Foodies.Datas.Entities.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Evlow_Foodies.Buisness.DTO
{
    public class RecipeIngredientDTO
    {
        public int RecipeId { get; set; }
        public int IngredientId { get; set; }
        public int? UnityId { get; set; }
        public decimal? Quantity { get; set; }

    }
}
