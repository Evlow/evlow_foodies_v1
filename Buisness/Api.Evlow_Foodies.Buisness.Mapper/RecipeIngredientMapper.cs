﻿using Api.Evlow_Foodies.Buisness.DTO;
using Api.Evlow_Foodies.Datas.Entities.Entities;

namespace Api.Evlow_Foodies.Buisness.Mapper
{
    public static class RecipeIngredientMapper
    {
        public static RecipeIngredient TransformDTOToEntity(RecipeIngredientDTO recipeIngredientDTO)
        {
            return new RecipeIngredient()
            {
                RecipeId = recipeIngredientDTO.RecipeId,
                IngredientId = recipeIngredientDTO.IngredientId,
                UnityId = recipeIngredientDTO.UnityId,
            };
        }
        public static RecipeIngredientDTO TransformEntityToDTO(RecipeIngredient recipeIngredientEntity)
        {
            return new RecipeIngredientDTO()
            {
                RecipeId = recipeIngredientEntity.RecipeId,
                IngredientId = recipeIngredientEntity.IngredientId,
                UnityId = recipeIngredientEntity.UnityId,

            };
        }
    }
}