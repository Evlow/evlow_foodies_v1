﻿using Api.Evlow_Foodies.Buisness.DTO;
using Api.Evlow_Foodies.Buisness.Mapper;
using Api.Evlow_Foodies.Buisness.Service.Contract;
using Api.Evlow_Foodies.Datas.Entities.Entities;
using Api.Evlow_Foodies.Datas.Repository.Contract;


namespace Api.Evlow_Foodies.Buisness.Service
{
    public class IngredientService : IIngredientService
    {
        /// <summary>
        /// Le repository de gestion des unités de mesures
        /// </summary>
        private readonly IIngredientRepository _ingredientRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="IngredientService"/> class.
        /// </summary>
        /// <param title="unityRepository">The unite repository.</param>
        public IngredientService(IIngredientRepository ingredientRepository)
        {
            _ingredientRepository = ingredientRepository;
        }

        /// <summary>
        /// Cette méthode permet de récupérer les listes des unités de mesure.
        /// </summary>
        /// <returns></returns>
        public async Task<List<Ingredient>> GetIngredientAsync()
        {
            var ingredients = await _ingredientRepository.GetIngredientsAsync().ConfigureAwait(false);
            List<Ingredient> listIngredient = new List<Ingredient>(ingredients.Count);

            foreach (var ingredient in ingredients)
            {
                listIngredient.Add(ingredient);
            }

            return listIngredient;
        }
        //DTO
        public async Task<List<IngredientDTO>> GetIngredientDTOAsync()
        {
            var ingredients = await _ingredientRepository.GetIngredientsAsync().ConfigureAwait(false);
            List<IngredientDTO> listIngredientDTO = new List<IngredientDTO>(ingredients.Count);

            foreach (var ingredient in ingredients)
            {
                listIngredientDTO.Add(IngredientMapper.TransformEntityToDTO(ingredient));
            }

            return listIngredientDTO;
        }


        public async Task<Ingredient> GetIngredientIdAsync(int ingredientId)
        {
            var ingredientGet = await _ingredientRepository.GetIngredientByIdAsync(ingredientId).ConfigureAwait(false);

             return ingredientGet;
        }
        //DTO
        public async Task<IngredientDTO> GetIngredientIdDTOAsync(int ingredientId)
        {
            var ingredientGet= await _ingredientRepository.GetIngredientByIdAsync(ingredientId).ConfigureAwait(false);

            var ingredientGetDTO = IngredientMapper.TransformEntityToDTO(ingredientGet);

            return ingredientGetDTO;
        }



        /// <summary>
        /// Cette méthode permet de créer une unité de mesure.
        /// </summary>
        /// <param title="unity">L'unité à créer.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Il existe déjà une unité de mesure du même nom !!</exception>
        public async Task<Ingredient> CreateIngredientAsync(Ingredient ingredient)
        {
            var isExiste = await CheckIngredientNameExisteAsync(ingredient.IngredientName).ConfigureAwait(false);
            if (isExiste)
                throw new Exception("Il existe déjà une unité de mesure du même nom !!");

            var ingredientToAdd = (ingredient);

            var ingredientAdded = await _ingredientRepository.CreateIngredientAsync(ingredientToAdd).ConfigureAwait(false);

            return ingredientAdded;
        }
        //DTO
        public async Task<IngredientDTO> CreateIngredientAsync(IngredientDTO ingredient)
        {
            var isExiste = await CheckIngredientNameExisteAsync(ingredient.IngredientName).ConfigureAwait(false);
            if (isExiste)
                throw new Exception("Il existe déjà une unité de mesure du même nom !!");

            var ingredientToAdd = IngredientMapper.TransformDTOToEntity(ingredient);

            var ingredientAdded = await _ingredientRepository.CreateIngredientAsync(ingredientToAdd).ConfigureAwait(false);

            return IngredientMapper.TransformEntityToDTO(ingredientAdded);
        }

        /// <summary>
        /// Cette méthode permet de mettre à jour une unité de mesure .
        /// </summary>
        /// <param title="UnityId">l'identifiant de unité</param>
        /// <param title="unity">l'unité modifié</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">
        /// Il existe déjà une unité de mesure du même nom !!
        /// or
        /// Il n'existe aucune unité de mesure avec cet identifiant : {UnityId}
        /// </exception>
        public async Task<Ingredient> UpdateIngredientAsync(int ingredientId, Ingredient ingredient)
        {
            var isExiste = await CheckIngredientNameExisteAsync(ingredient.IngredientName).ConfigureAwait(false);
            if (isExiste)
                throw new Exception("Il existe déjà une unité de mesure du même nom !!");

            var ingredientGet = await _ingredientRepository.GetIngredientByIdAsync(ingredientId).ConfigureAwait(false);
            if (ingredientGet == null)
                throw new Exception($"Il n'existe aucune unité de mesure avec cet identifiant : {ingredientId}");

            ingredientGet.IngredientName = ingredient.IngredientName;

            var ingredientUpdated = await _ingredientRepository.UpdateIngredientAsync(ingredientGet).ConfigureAwait(false);

            return ingredientUpdated;
        }
        //DTO
        public async Task<IngredientDTO> UpdateIngredientAsync(int ingredientId, IngredientDTO ingredientdto)
        {
            var isExiste = await CheckIngredientNameExisteAsync(ingredientdto.IngredientName).ConfigureAwait(false);
            if (isExiste)
                throw new Exception("Il existe déjà une unité de mesure du même nom !!");

            var ingredientGet = await _ingredientRepository.GetIngredientByIdAsync(ingredientId).ConfigureAwait(false);
            if (ingredientGet == null)
                throw new Exception($"Il n'existe aucune unité de mesure avec cet identifiant : {ingredientId}");

            ingredientGet.IngredientName = ingredientdto.IngredientName;

            var ingredientUpdated = await _ingredientRepository.UpdateIngredientAsync(ingredientGet).ConfigureAwait(false);

            return IngredientMapper.TransformEntityToDTO(ingredientUpdated);
        }
        /// <summary>
        /// Cette méthode permet de supprimer une unité de mesure.
        /// </summary>
        /// <param title="ingredientId">L'identifiant de l'unité à supprimer.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Il n'existe aucune unité de mesure avec cet identifiant : {idUnite}</exception>
        public async Task<Ingredient> DeleteIngredientAsync(int ingredientId)
        {
            var ingredientGet = await _ingredientRepository.GetIngredientByIdAsync(ingredientId).ConfigureAwait(false);
            if (ingredientGet == null)
                throw new Exception($"Il n'existe aucune unité de mesure avec cet identifiant : {ingredientId}");

            var ingredientDeleted = await _ingredientRepository.DeleteIngredientAsync(ingredientGet).ConfigureAwait(false);

            return ingredientDeleted;
        }
        //DTO
        public async Task<IngredientDTO> DeleteIngredientDTOAsync(int ingredientId)
        {
            var ingredientGet = await _ingredientRepository.GetIngredientByIdAsync(ingredientId).ConfigureAwait(false);
            if (ingredientGet == null)
                throw new Exception($"Il n'existe aucune unité de mesure avec cet identifiant : {ingredientId}");

            var ingredientDeleted = await _ingredientRepository.DeleteIngredientAsync(ingredientGet).ConfigureAwait(false);


            return IngredientMapper.TransformEntityToDTO(ingredientDeleted);
        }


        /// <summary>
        /// Cette méthode permet de vérifier si une unité existe déjà avec le même nom.
        /// </summary>
        /// <param title="ingredientName">le nom de l'unité.</param>
        private async Task<bool> CheckIngredientNameExisteAsync(string ingredientName)
        {
            var ingredientGet = await _ingredientRepository.GetIngredientByNameAsync(ingredientName).ConfigureAwait(false);

            return ingredientGet != null;
        }

        //DTO


    }
}
