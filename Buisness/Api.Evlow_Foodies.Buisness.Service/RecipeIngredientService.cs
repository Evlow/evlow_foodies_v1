﻿using Api.Evlow_Foodies.Buisness.DTO;
using Api.Evlow_Foodies.Buisness.Mapper;
using Api.Evlow_Foodies.Buisness.Service.Contract;
using Api.Evlow_Foodies.Datas.Entities.Entities;
using Api.Evlow_Foodies.Datas.Repository.Contract;


namespace Api.Evlow_Foodies.Buisness.Service
{
    public class RecipeIngredientService : IRecipeIngredientService
    {
        /// <summary>
        /// Le repository de gestion des unités de mesures
        /// </summary>
        private readonly IRecipeIngredientRepository _recipeIngredientRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="RecipeIngredientService"/> class.
        /// </summary>
        /// <param name="unityRepository">The unite repository.</param>
        public RecipeIngredientService(IRecipeIngredientRepository recipeIngredientRepository)
        {
            _recipeIngredientRepository = recipeIngredientRepository;
        }

        /// <summary>
        /// Cette méthode permet de récupérer les listes des unités de mesure.
        /// </summary>
        /// <returns></returns>
        public async Task<List<RecipeIngredientDTO>> GetRecipeIngredientAsync()
        {
            var recipeIngredients = await _recipeIngredientRepository.GetRecipeIngredientsAsync().ConfigureAwait(false);
            List<RecipeIngredientDTO> listRecipeIngredient = new (recipeIngredients.Count);

            foreach (var recipeIngredient in recipeIngredients)
            {
                listRecipeIngredient.Add(RecipeIngredientMapper.TransformEntityToDTO(recipeIngredient));
            }

            return listRecipeIngredient;
        }


        public async Task<RecipeIngredientDTO> GetRecipeIngredientIdAsync(int recipeIngredientId)
        {
            var recipeIngredientGet = await _recipeIngredientRepository.GetRecipeIngredientByIdAsync(recipeIngredientId).ConfigureAwait(false);
            var recipeIngredientGetDTO = RecipeIngredientMapper.TransformEntityToDTO(recipeIngredientGet);

            return recipeIngredientGetDTO;
        }



        /// <summary>
        /// Cette méthode permet de créer une unité de mesure.
        /// </summary>
        /// <param name="unity">L'unité à créer.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Il existe déjà une unité de mesure du même nom !!</exception>
        public async Task<RecipeIngredientDTO> CreateRecipeIngredientAsync(RecipeIngredientDTO recipeIngredient)
        {
            var recipeIngredientToAdd = RecipeIngredientMapper.TransformDTOToEntity(recipeIngredient);

            var recipeIngredientAdded = await _recipeIngredientRepository.CreateRecipeIngredientAsync(recipeIngredientToAdd).ConfigureAwait(false);

            return RecipeIngredientMapper.TransformEntityToDTO(recipeIngredientAdded);
        }

        /// <summary>
        /// Cette méthode permet de mettre à jour une unité de mesure .
        /// </summary>
        /// <param name="UnityId">l'identifiant de unité</param>
        /// <param name="unity">l'unité modifié</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">
        /// Il existe déjà une unité de mesure du même nom !!
        /// or
        /// Il n'existe aucune unité de mesure avec cet identifiant : {UnityId}
        /// </exception>
        public async Task<RecipeIngredientDTO> UpdateRecipeIngredientAsync(int recipeIngredientId, RecipeIngredientDTO recipeIngredient)
        {

            var recipeIngredientGet = await _recipeIngredientRepository.GetRecipeIngredientByIdAsync(recipeIngredientId).ConfigureAwait(false);
            if (recipeIngredientGet == null)
                throw new Exception($"Il n'existe aucune unité de mesure avec cet identifiant : {recipeIngredientId}");

            recipeIngredientGet.RecipeIngredientQuantity = recipeIngredient.Quantity;

            var recipeIngredientUpdated = await _recipeIngredientRepository.UpdateRecipeIngredientAsync(recipeIngredientGet).ConfigureAwait(false);

            return RecipeIngredientMapper.TransformEntityToDTO(recipeIngredientUpdated);
        }

        /// <summary>
        /// Cette méthode permet de supprimer une unité de mesure.
        /// </summary>
        /// <param name="recipeIngredientId">L'identifiant de l'unité à supprimer.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Il n'existe aucune unité de mesure avec cet identifiant : {idUnite}</exception>
        public async Task<RecipeIngredientDTO> DeleteRecipeIngredientAsync(int recipeIngredientId)
        {
            var recipeIngredientGet = await _recipeIngredientRepository.GetRecipeIngredientByIdAsync(recipeIngredientId).ConfigureAwait(false);
            if (recipeIngredientGet == null)
                throw new Exception($"Il n'existe aucune unité de mesure avec cet identifiant : {recipeIngredientId}");

            var recipeIngredientDeleted = await _recipeIngredientRepository.DeleteRecipeIngredientAsync(recipeIngredientGet).ConfigureAwait(false);

            return RecipeIngredientMapper.TransformEntityToDTO(recipeIngredientDeleted);
        }

        /// <summary>
        /// Cette méthode permet de vérifier si une unité existe déjà avec le même nom.
        /// </summary>
        /// <param name="recipeIngredientName">le nom de l'unité.</param>
        //private async Task<bool> CheckRecipeIngredientQuantityExisteAsync(decimal recipeIngredientQuantity)
        //{
        //    var recipeIngredientGet = await _recipeIngredientRepository.GetRecipeIngredientByQuantityAsync(recipeIngredientQuantity).ConfigureAwait(false);

        //    return recipeIngredientGet != null;
        //}
    }
}
