﻿using Api.Evlow_Foodies.Buisness.DTO;
using Api.Evlow_Foodies.Buisness.Mapper;
using Api.Evlow_Foodies.Buisness.Service.Contract;
using Api.Evlow_Foodies.Datas.Entities.Entities;
using Api.Evlow_Foodies.Datas.Repository.Contract;


namespace Api.Evlow_Foodies.Buisness.Service
{
    public class FavoriService : IFavoriService
    {
        /// <summary>
        /// Le repository de gestion des unités de mesures
        /// </summary>
        private readonly IFavoriRepository _favoriRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="FavoriService"/> class.
        /// </summary>
        /// <param name="unityRepository">The unite repository.</param>
        public FavoriService(IFavoriRepository favoriRepository)
        {
            _favoriRepository = favoriRepository;
        }

        /// <summary>
        /// Cette méthode permet de récupérer les listes des unités de mesure.
        /// </summary>
        /// <returns></returns>
        public async Task<List<FavoriDTO>> GetFavoriAsync()
        {
            var favoris = await _favoriRepository.GetFavorisAsync().ConfigureAwait(false);
            List<FavoriDTO> listFavori = new List<FavoriDTO>(favoris.Count);

            foreach (var favori in favoris)
            {
                listFavori.Add(FavorisMapper.TransformEntityToDTO(favori));
            }

            return listFavori;
        }


        public async Task<FavoriDTO> GetFavoriIdAsync(int favoriId)
        {
            var favoriGet = await _favoriRepository.GetFavoriByIdAsync(favoriId).ConfigureAwait(false);
            var favoriGetDTO = FavorisMapper.TransformEntityToDTO(favoriGet);

            return favoriGetDTO;
        }


        /// <summary>
        /// Cette méthode permet de mettre à jour une unité de mesure .
        /// </summary>
        /// <param name="UnityId">l'identifiant de unité</param>
        /// <param name="unity">l'unité modifié</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">
        /// Il existe déjà une unité de mesure du même nom !!
        /// or
        /// Il n'existe aucune unité de mesure avec cet identifiant : {UnityId}
        /// </exception>
        //public async Task<Favori> UpdateFavoriAsync(int favoriId, Favori favori)
        //{

        //    var favoriGet = await _favoriRepository.GetFavoriByIdAsync(favoriId).ConfigureAwait(false);
        //    if (favoriGet == null)
        //        throw new Exception($"Il n'existe aucune unité de mesure avec cet identifiant : {favoriId}");

        //    var favoriUpdated = await _favoriRepository.UpdateFavoriAsync(favori.FavorisId).ConfigureAwait(false);

        //    return favoriUpdated;
        //}

        /// <summary>
        /// Cette méthode permet de supprimer une unité de mesure.
        /// </summary>
        /// <param name="favoriId">L'identifiant de l'unité à supprimer.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">Il n'existe aucune unité de mesure avec cet identifiant : {idUnite}</exception>
        public async Task<FavoriDTO> DeleteFavoriAsync(int favoriId)
        {
            var favoriGet = await _favoriRepository.GetFavoriByIdAsync(favoriId).ConfigureAwait(false);
            if (favoriGet == null)
                throw new Exception($"Il n'existe aucune unité de mesure avec cet identifiant : {favoriId}");

            var favoriDeleted = await _favoriRepository.DeleteFavoriAsync(favoriGet).ConfigureAwait(false);

            return FavorisMapper.TransformEntityToDTO(favoriDeleted);
        }


    }
}
